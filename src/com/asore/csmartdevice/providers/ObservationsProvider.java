package com.asore.csmartdevice.providers;

import com.asore.csmartdevice.database.CSmartDeviceSQLiteHelper;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class ObservationsProvider extends ContentProvider {

	private static final String uri = "content://smartdevice.observations.provider/observations";
	public static final Uri CONTENT_URI = Uri.parse(uri);
	private static final int ACCESS_OBSERVATIONS = 1;
	private static final int ACCESS_OBSERVATIONS_ID = 2;
	private static final String OBSERVATIONS_TABLE = "Observations";
	private static final UriMatcher uriMatcher;
	private static final String DBName = "CSmartDevices";
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("smartdevice.observations.provider", "observations",
				ACCESS_OBSERVATIONS);
		uriMatcher.addURI("smartdevice.observations.provider",
				"observations/#", ACCESS_OBSERVATIONS_ID);
	}
	private CSmartDeviceSQLiteHelper dbHelper;
	private SQLiteDatabase db;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int cont = 0;
		String id = selection;
		if (uriMatcher.match(uri) == ACCESS_OBSERVATIONS_ID) {
			id = "_id=" + uri.getLastPathSegment();
			cont = db.delete(OBSERVATIONS_TABLE, id, selectionArgs);
		}
		return cont;
	}

	@Override
	public String getType(Uri uri) {
		int match = uriMatcher.match(uri);

		switch (match) {
		case ACCESS_OBSERVATIONS:
			return "vnd.android.cursor.dir/vnd.smartdevice.observations.provider";
		case ACCESS_OBSERVATIONS_ID:
			return "vnd.android.cursor.item/vnd.smartdevice.observations.provider";
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long regId;
		Uri newUri = null;
		if (uriMatcher.match(uri) == ACCESS_OBSERVATIONS) {
			regId = db.insert(OBSERVATIONS_TABLE, null, values);
			newUri = ContentUris.withAppendedId(CONTENT_URI, regId);
		}
		return newUri;
	}

	@Override
	public boolean onCreate() {
		dbHelper = new CSmartDeviceSQLiteHelper(getContext(), DBName, null, 1);
		db = dbHelper.getWritableDatabase();
		return db != null && db.isOpen();
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor c = null;
		if (uriMatcher.match(uri) == ACCESS_OBSERVATIONS) {
			c = db.query(OBSERVATIONS_TABLE, projection, null, null, null,
					null, null);
		}
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
