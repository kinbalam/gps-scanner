package com.asore.csmartdevice.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.asore.csmartdevice.database.CSmartDeviceSQLiteHelper;

public class SmartActivitiesProvider extends ContentProvider {
	
	private static final String uri_smartactivities = "content://smartdevice.smartactivities.provider/smartactivities";
	public static final Uri CONTENT_URI_SMART_ACTIVITIES = Uri.parse(uri_smartactivities);
	// UriMatcher
	private static final UriMatcher uriMatcher;
	private static final int ACCESS_SMART_ACTIVITYES = 7;
	private static final int ACCESS_SMART_ACTIVITYES_ID = 8;
	private static final String SMART_ACTIVITIES_TABLE = "SmartActivities";
	private static final String DBName = "CSmartDevices";
	// Inicializamos el UriMatcher
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("smartdevice.smartactivities.provider", "smartactivities",
				ACCESS_SMART_ACTIVITYES);
		uriMatcher.addURI("smartdevice.smartactivities.provider", "smartactivities/#",
				ACCESS_SMART_ACTIVITYES_ID);
	}
	private CSmartDeviceSQLiteHelper dbHelper;
	private SQLiteDatabase db;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int cont = 0;
		String id = selection;
		// if(uriMatcher.match(uri) == ACCESS_OBSERVATIONS_ID){
		// id = "_id=" + uri.getLastPathSegment();
		// cont = db.delete(OBSERVATIONS_TABLE, id, selectionArgs);
		// }
		if (uriMatcher.match(uri) == ACCESS_SMART_ACTIVITYES_ID) {
			id = "_id=" + uri.getLastPathSegment();
			cont = db.delete(SMART_ACTIVITIES_TABLE, id, selectionArgs);
		}
		return cont;
	}

	@Override
	public String getType(Uri uri) {
		int match = uriMatcher.match(uri);

		switch (match) {
		case ACCESS_SMART_ACTIVITYES:
			return "vnd.android.cursor.dir/vnd.smartdevice.smartactivities.provider";
		case ACCESS_SMART_ACTIVITYES_ID:
			return "vnd.android.cursor.item/vnd.smartdevice.smartactivities.provider";
			// case ACCESS_OBSERVATIONS:
			// return "vnd.android.cursor.dir/vnd.csmartdevice.observation";
			// case ACCESS_OBSERVATIONS_ID:
			// return "vnd.android.cursor.item/vnd.csmartdevice.observation";
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long regId;
		Uri newUri = null;
		// if (uriMatcher.match(uri) == ACCESS_OBSERVATIONS) {
		// regId = db.insert(OBSERVATIONS_TABLE, null, values);
		// // newUri = ContentUris.withAppendedId(CONTENT_URI, regId);
		// }
		if (uriMatcher.match(uri) == ACCESS_SMART_ACTIVITYES) {
			regId = db.insert(SMART_ACTIVITIES_TABLE, null, values);
			newUri = ContentUris.withAppendedId(CONTENT_URI_SMART_ACTIVITIES, regId);
		}
		return newUri;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		dbHelper = new CSmartDeviceSQLiteHelper(getContext(), DBName, null, 1);
		db = dbHelper.getWritableDatabase();
		return db != null && db.isOpen();
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor c = null;
		String where = selection;
		if(uriMatcher.match(uri) ==ACCESS_SMART_ACTIVITYES_ID){
            where = "idListActivity=" + uri.getLastPathSegment();
        }
			c = db.query(SMART_ACTIVITIES_TABLE, projection, where, null, null, null,
					null);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {

		return 0;
	}

}

