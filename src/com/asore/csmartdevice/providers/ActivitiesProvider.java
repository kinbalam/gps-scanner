package com.asore.csmartdevice.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.asore.csmartdevice.database.CSmartDeviceSQLiteHelper;

public class ActivitiesProvider extends ContentProvider {
	
	private static final String uri_activities = "content://smartdevice.activities.provider/activities";
	public static final Uri CONTENT_URI_ACTIVITIES = Uri.parse(uri_activities);
	// UriMatcher
	private static final UriMatcher uriMatcher;
	private static final int ACCESS_ACTIVITYES = 3;
	private static final int ACCESS_ACTIVITYES_ID = 4;
	private static final String ACTIVITIES_TABLE = "Activities";
	private static final String DBName = "CSmartDevices";
	// Inicializamos el UriMatcher
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("smartdevice.activities.provider", "activities",
				ACCESS_ACTIVITYES);
		uriMatcher.addURI("smartdevice.activities.provider", "activities/#",
				ACCESS_ACTIVITYES_ID);
	}
	private CSmartDeviceSQLiteHelper dbHelper;
	private SQLiteDatabase db;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int cont = 0;
		String id = selection;
		// if(uriMatcher.match(uri) == ACCESS_OBSERVATIONS_ID){
		// id = "_id=" + uri.getLastPathSegment();
		// cont = db.delete(OBSERVATIONS_TABLE, id, selectionArgs);
		// }
		if (uriMatcher.match(uri) == ACCESS_ACTIVITYES_ID) {
			id = "_id=" + uri.getLastPathSegment();
			cont = db.delete(ACTIVITIES_TABLE, id, selectionArgs);
		}
		return cont;
	}

	@Override
	public String getType(Uri uri) {
		int match = uriMatcher.match(uri);

		switch (match) {
		case ACCESS_ACTIVITYES:
			return "vnd.android.cursor.dir/vnd.smartdevice.activities.provider";
		case ACCESS_ACTIVITYES_ID:
			return "vnd.android.cursor.item/vnd.smartdevice.activities.provider";
			// case ACCESS_OBSERVATIONS:
			// return "vnd.android.cursor.dir/vnd.csmartdevice.observation";
			// case ACCESS_OBSERVATIONS_ID:
			// return "vnd.android.cursor.item/vnd.csmartdevice.observation";
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long regId;
		Uri newUri = null;
		// if (uriMatcher.match(uri) == ACCESS_OBSERVATIONS) {
		// regId = db.insert(OBSERVATIONS_TABLE, null, values);
		// // newUri = ContentUris.withAppendedId(CONTENT_URI, regId);
		// }
		if (uriMatcher.match(uri) == ACCESS_ACTIVITYES) {
			regId = db.insert(ACTIVITIES_TABLE, null, values);
			newUri = ContentUris.withAppendedId(CONTENT_URI_ACTIVITIES, regId);
		}
		return newUri;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		dbHelper = new CSmartDeviceSQLiteHelper(getContext(), DBName, null, 1);
		db = dbHelper.getWritableDatabase();
		return db != null && db.isOpen();
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor c = null;
		// if(uriMatcher.match(uri) == ACCESS_OBSERVATIONS){
		// c = db.query(OBSERVATIONS_TABLE, projection, null,
		// null, null, null, null);
		// }
		if (uriMatcher.match(uri) == ACCESS_ACTIVITYES) {
			c = db.query(ACTIVITIES_TABLE, projection, null, null, null, null,
					null);
		}
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {

		return 0;
	}

}
