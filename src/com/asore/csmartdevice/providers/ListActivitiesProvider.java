package com.asore.csmartdevice.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.asore.csmartdevice.database.CSmartDeviceSQLiteHelper;

public class ListActivitiesProvider extends ContentProvider{
	private static final String uri_listActivities = "content://smartdevice.listActivities.provider/listActivities";
	public static final Uri CONTENT_URI_LISTACTIVITIES = Uri.parse(uri_listActivities);
	// UriMatcher
	private static final UriMatcher uriMatcher;
	private static final int ACCESS_LISTACTIVITYES = 5;
	private static final int ACCESS_LISTACTIVITYES_ID = 6;
	private static final String LISTACTIVITIES_TABLE = "ListActivities";
	private static final String DBName = "CSmartDevices";
	// Inicializamos el UriMatcher
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("smartdevice.listActivities.provider", "listActivities",
				ACCESS_LISTACTIVITYES);
		uriMatcher.addURI("smartdevice.listActivities.provider", "listActivities/#",
				ACCESS_LISTACTIVITYES_ID);
	}
	private CSmartDeviceSQLiteHelper dbHelper;
	private SQLiteDatabase db;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int cont = 0;
		String id = selection;
		// if(uriMatcher.match(uri) == ACCESS_OBSERVATIONS_ID){
		// id = "_id=" + uri.getLastPathSegment();
		// cont = db.delete(OBSERVATIONS_TABLE, id, selectionArgs);
		// }
		if (uriMatcher.match(uri) == ACCESS_LISTACTIVITYES_ID) {
			id = "_id=" + uri.getLastPathSegment();
			cont = db.delete(LISTACTIVITIES_TABLE, id, selectionArgs);
		}
		return cont;
	}

	@Override
	public String getType(Uri uri) {
		int match = uriMatcher.match(uri);

		switch (match) {
		case ACCESS_LISTACTIVITYES:
			return "vnd.android.cursor.dir/vnd.smartdevice.listActivities.provider";
		case ACCESS_LISTACTIVITYES_ID:
			return "vnd.android.cursor.item/vnd.smartdevice.listActivities.provider";
			// case ACCESS_OBSERVATIONS:
			// return "vnd.android.cursor.dir/vnd.csmartdevice.observation";
			// case ACCESS_OBSERVATIONS_ID:
			// return "vnd.android.cursor.item/vnd.csmartdevice.observation";
		default:
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long regId;
		Uri newUri = null;
		// if (uriMatcher.match(uri) == ACCESS_OBSERVATIONS) {
		// regId = db.insert(OBSERVATIONS_TABLE, null, values);
		// // newUri = ContentUris.withAppendedId(CONTENT_URI, regId);
		// }
		if (uriMatcher.match(uri) == ACCESS_LISTACTIVITYES) {
			regId = db.insert(LISTACTIVITIES_TABLE, null, values);
			newUri = ContentUris.withAppendedId(CONTENT_URI_LISTACTIVITIES, regId);
		}
		return newUri;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		dbHelper = new CSmartDeviceSQLiteHelper(getContext(), DBName, null, 1);
		db = dbHelper.getWritableDatabase();
		return db != null && db.isOpen();
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Cursor c = null;
		// if(uriMatcher.match(uri) == ACCESS_OBSERVATIONS){
		// c = db.query(OBSERVATIONS_TABLE, projection, null,
		// null, null, null, null);
		// }
		if (uriMatcher.match(uri) == ACCESS_LISTACTIVITYES) {
			c = db.query(LISTACTIVITIES_TABLE, projection, null, null, null, null,
					null);
		}
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {

		return 0;
	}
}
