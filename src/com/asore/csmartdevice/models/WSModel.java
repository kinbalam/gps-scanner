package com.asore.csmartdevice.models;

import java.io.Serializable;

@SuppressWarnings("serial")
public class WSModel implements Serializable {

	private String namespace;
	private String url;
	private String method_name;
	private String soap_action;
	private int interval_coordinates;
	private int ID;

	public WSModel(int id, String namespace, String url, String method_name,
			String soap_action, int interval_coordinates) {
		super();
		this.ID = id;
		this.namespace = namespace;
		this.url = url;
		this.method_name = method_name;
		this.soap_action = soap_action;
		this.interval_coordinates = interval_coordinates;
	}

	public WSModel(int id, String namespace, String url, String method_name,
			String soap_action) {
		this.ID = id;
		this.namespace = namespace;
		this.url = url;
		this.method_name = method_name;
		this.soap_action = soap_action;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod_name() {
		return method_name;
	}

	public void setMethod_name(String method_name) {
		this.method_name = method_name;
	}

	public String getSoap_action() {
		return soap_action;
	}

	public void setSoap_action(String soap_action) {
		this.soap_action = soap_action;
	}

	public int getInterval_coordinates() {
		return interval_coordinates;
	}

	public void setInterval_coordinates(int interval_coordinates) {
		this.interval_coordinates = interval_coordinates;
	}

}
