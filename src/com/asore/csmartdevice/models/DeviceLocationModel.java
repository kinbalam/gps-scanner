package com.asore.csmartdevice.models;

public class DeviceLocationModel {

	private double latitude;
	private double longitude;
	private String date;
	private String imeiDevice;
	private String numOpr;

	public DeviceLocationModel(double latitude, double longitude, String date,
			String imeiDevice, String numOpr) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.date = date;
		this.imeiDevice = imeiDevice;
		this.numOpr = numOpr;
	}

	public String getNumOpr() {
		return numOpr;
	}

	public void setNumOpr(String numOpr) {
		this.numOpr = numOpr;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getImeiDevice() {
		return imeiDevice;
	}

	public void setImeiDevice(String imeiDevice) {
		this.imeiDevice = imeiDevice;
	}

}
