package com.asore.csmartdevice.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.asore.csmartdevice.adapters.CSmartDeviceDbAdapter;
import com.asore.csmartdevice.models.DeviceLocationModel;
import com.asore.csmartdevice.models.WSModel;

public class GPSTrackerService extends Service implements LocationListener {

	int variable = 1;
	boolean isGPSEnabled = false;
	boolean isNetworkEnabled = false;
	boolean canGetLocation = false;
	private Location location;
	double latitude;
	double longitude;
	private CSmartDeviceDbAdapter dbAdapter;
	private double lastLatitude = 0;
	private double lastLongitude = 0;
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
	private long MIN_TIME_BW_UPDATES = 1000;
	protected LocationManager locationManager;

	public Location getLocation() {
		try {
			locationManager = (LocationManager) this
					.getSystemService(LOCATION_SERVICE);
			int seconds_updates = getSecondsForDefaultWS();
			Log.i("segundos del ws", "" + seconds_updates);
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
//				Toast.makeText(getApplicationContext(), "gps y red apagados",
//						Toast.LENGTH_SHORT).show();
			} else {
				this.canGetLocation = true;
				if (isNetworkEnabled) {			
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES * seconds_updates,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.d("Network", "Network");
					if (locationManager != null) {
						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							System.out.println("Red:" + latitude + ":"
									+ longitude);
						}
					}
				}

				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES * seconds_updates,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						Log.i("GPS Enabled", "GPS Enabled");
						if (locationManager != null) {
							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null) {
								latitude = location.getLatitude();
								longitude = location.getLongitude();
								System.out.println("gps:" + latitude + ":"
										+ longitude);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return location;
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 * */
	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(GPSTrackerService.this);
		}
	}

	/**
	 * Function to get latitude
	 * */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}

		return latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}

		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 * 
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog On pressing Settings button will
	 * lauch Settings Options
	 * */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		alertDialog.setTitle("GPS is settings");

		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);
					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

	@Override
	public void onCreate() {
		Log.i("Start service GPS", "Service running");
		dbAdapter = new CSmartDeviceDbAdapter(this);
		dbAdapter.openDataBase();
		getLocation();
	}

	@Override
	public void onStart(Intent intent, int startid) {

	}

	public void onLocationChanged(Location location) {
		final SharedPreferences prefs = getSharedPreferences("MisPreferencias",
				Context.MODE_PRIVATE);
		String enable = prefs.getString("enable", "1");
		if (enable.equals("1")) {
			Log.i("Enable", "1");
			Log.i("Localizacion anterior: ", "LastLat: " + lastLatitude
					+ " LastLong: " + lastLongitude);
			Log.i("Localizacion actual: ", "Lat: " + location.getLatitude()
					+ " Long: " + location.getLongitude());
			if (lastLatitude != location.getLatitude()
					|| lastLongitude != location.getLongitude()) {
				lastLatitude = location.getLatitude();
				lastLongitude = location.getLongitude();
				DeviceLocationModel device_location = getDeviceLocationModel(location);
				ContentValues newLocation = new ContentValues();
				newLocation.put("latitud", device_location.getLatitude());
				newLocation.put("longitud", device_location.getLongitude());
				newLocation.put("imei", device_location.getImeiDevice());
				newLocation.put("fecha", device_location.getDate());
				newLocation.put("numOpr", device_location.getNumOpr());
				Cursor cursorObservation = dbAdapter.getObservations();
				String activituyId = "";
				String obs = "";
				int idObservation = 0;
				while(cursorObservation.moveToNext()){
					idObservation = cursorObservation.getInt(0);
					activituyId = cursorObservation.getString(1);
					obs = cursorObservation.getString(2);
					}
				newLocation.put("activityType", activituyId);
				newLocation.put("observation", obs);
				long register = dbAdapter.registerNewLocation(newLocation);
				if(idObservation != 0){
					dbAdapter.deleteObservation(idObservation);	
				}
				sendLocationToWebService();
				Cursor cursor = dbAdapter.getLocationsCursor();
				if (register == -1) {
					Toast.makeText(this, "No se pudo registrar en la tabla.",
							Toast.LENGTH_LONG).show();
					Log.i("Error inserting rows", "no se pudo registrar en la tabla");
				} else {
					Log.i("Number of rows", "" + cursor.getCount());
//					Toast.makeText(this, "Location registered.",
//							Toast.LENGTH_LONG).show();
				}
			}
		} else {
			Log.i("Enable", "0");
//			Toast.makeText(this, "Location no registered.", Toast.LENGTH_LONG)
//					.show();
			
		}

	}

	public void onProviderDisabled(String provider) {
//		Toast.makeText(this, "GPS Disable", Toast.LENGTH_LONG).show();
	}

	public void onProviderEnabled(String provider) {
//		Toast.makeText(this, "GPS Enable", Toast.LENGTH_LONG).show();
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.i("Status GPS", String.valueOf(status));
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@SuppressLint("SimpleDateFormat")
	private DeviceLocationModel getDeviceLocationModel(Location location) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		double latitude = location.getLatitude();
		double longitude = location.getLongitude();
		String date = sdf.format(new Date());
		String imeiDevice = getIMEIDevice();
		String numOpr = hashMD5(date);
		return new DeviceLocationModel(latitude, longitude, date, imeiDevice,
				numOpr);
	}

	private String hashMD5(String string) {

		try {
			// .s. Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(string.getBytes());
			byte messageDigest[] = digest.digest();

			// .s. Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return string.replaceAll("[.:/,%?#&=]", "");
	}

	private String getIMEIDevice() {
		final TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if (mTelephony.getDeviceId() != null) {
			String phoneImei = mTelephony.getDeviceId();
			return phoneImei;
		} else {
			String tabletImei = Secure.getString(getApplicationContext()
					.getContentResolver(), Secure.ANDROID_ID);
			return tabletImei;
		}
	}

	private int getSecondsForDefaultWS() {
//		dbAdapter.openDataBase();
		WSModel ws = dbAdapter.getDefaultWebService();
//		dbAdapter.closeConnectionDB();
		return ws.getInterval_coordinates();
	}

	private void sendLocationToWebService() {
		Intent broadCastIntent = new Intent();
		broadCastIntent
				.setAction("com.views.csmartdevice.SendLocationReceiver");
		sendBroadcast(broadCastIntent);
	}

	 @Override
	 public void onDestroy() {
	 // TODO Auto-generated method stub
		 Log.i("Service GPS Stopped", "Service stopped");
		 dbAdapter.closeConnectionDB();
	 super.onDestroy();
	 stopUsingGPS();
	 stopSelf();
	 }
}
