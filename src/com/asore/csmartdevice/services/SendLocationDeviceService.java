package com.asore.csmartdevice.services;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.asore.csmartdevice.adapters.CSmartDeviceDbAdapter;
import com.asore.csmartdevice.models.WSModel;

public class SendLocationDeviceService extends IntentService {
	private WSModel wsModel;
	private Cursor cursor;
	private CSmartDeviceDbAdapter dbAdapter;

	public SendLocationDeviceService() {
		super("SendLocationDevice");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.i("TaskService", "Service running on intent service");
		initSendProcess();
		if (cursor.getCount() == 0) {
			Log.i("Stop service", "no rows");
			dbAdapter.closeConnectionDB();
			stopSelf();
		} else {
			// Modelo del transporte
			HttpTransportSE transporte = new HttpTransportSE(wsModel.getUrl());
			transporte.debug = true;
			// creacion del envelope (Proceso de envio al web service)
			try {
				SoapObject request = new SoapObject(wsModel.getNamespace(),
						wsModel.getMethod_name());
				SoapObject arrayRecords = new SoapObject(
						wsModel.getNamespace(), "Recordsd");
				while (cursor.moveToNext()) {
					SoapObject record = new SoapObject(wsModel.getNamespace(),
							"recordsd1");
					double latitude = cursor.getDouble(1);
					double longitude = cursor.getDouble(2);
					String imei = cursor.getString(3);
					String date = cursor.getString(4);
					String numOpr = cursor.getString(5);
					String activity = cursor.getString(6);
					String observation = cursor.getString(7);
					record.addPropertyIfValue("recordDevice", numOpr);
					record.addPropertyIfValue("recordLatitude",
							String.valueOf(latitude));
					record.addPropertyIfValue("recordLongitude",
							String.valueOf(longitude));
					record.addPropertyIfValue("recordImei", imei);
					record.addPropertyIfValue("recordDateTime", date);
					record.addPropertyIfValue("recordActividad", activity);
					record.addPropertyIfValue("recordObs", observation);
					arrayRecords.addSoapObject(record);
				}
				request.addSoapObject(arrayRecords);
				// Modelo intancial del Sobre
				SoapSerializationEnvelope sobre = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				sobre.setOutputSoapObject(request);
				// Llamada al ws
				transporte.call(wsModel.getSoap_action(), sobre);
				Log.i("Request: ", transporte.requestDump);
				// Resultado
				SoapObject resSoap = (SoapObject) sobre.getResponse();
				for (int index = 0; index < resSoap.getPropertyCount(); index++) {
					SoapObject respondeRecord = (SoapObject) resSoap
							.getProperty(index);
					String num_Opr = respondeRecord.getProperty(0).toString();
					int rowsAffected = dbAdapter.deleteLocation(num_Opr);
					if (rowsAffected == 1) {
						Log.i("Element deleted: ", num_Opr);

					}
					if (rowsAffected == 0) {
						Log.i("Element not deleted: ", "" + num_Opr);
					}
				}
				Log.i("Response: ", transporte.responseDump);
				Log.i("Rows afrer web service: ", ""
						+ dbAdapter.getLocationsCursor().getCount());
				dbAdapter.closeConnectionDB();
				stopSelf();
			} catch (Exception e) {
				Log.i("Error sending to ws", e.getMessage());
				dbAdapter.closeConnectionDB();
				stopSelf();
			}
		}
	}

	private void initSendProcess() {
		dbAdapter = new CSmartDeviceDbAdapter(this);
		dbAdapter.openDataBase();
		cursor = dbAdapter.getLocationsCursor();
		wsModel = dbAdapter.getDefaultWebService();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.i("Service status", "Service stopped");
		super.onDestroy();
	}
}
