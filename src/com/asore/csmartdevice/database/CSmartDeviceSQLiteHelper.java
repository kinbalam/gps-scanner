package com.asore.csmartdevice.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class CSmartDeviceSQLiteHelper extends SQLiteOpenHelper {

	private String SQL_WS_TABLE = "CREATE TABLE WebServices ("
			+ "_id INTEGER PRIMARY KEY AUTOINCREMENT, url "
			+ "TEXT, intervaloDeEnvio INTEGER, namespace TEXT, "
			+ "soap_action TEXT, method_name TEXT, def BOOLEAN)";

	private String SQL_LOCATION_TABLE = "CREATE TABLE Location ("
			+ "_id INTEGER PRIMARY KEY AUTOINCREMENT, latitud "
			+ "DOUBLE, longitud DOUBLE, imei TEXT,"
			+ "fecha TEXT, numOpr TEXT, activityType TEXT, observation TEXT)";
	
	private String SQL_ACTIVITIES_TABLE = "CREATE TABLE Activities ("
			+ "_id INTEGER PRIMARY KEY AUTOINCREMENT, activityType TEXT, activityDesc TEXT)";
	
	private String SQL_ACTIVITY_OBSERVATION_TABLE = "CREATE TABLE Observations ("
			+ "_id INTEGER PRIMARY KEY AUTOINCREMENT, activityType TEXT, observation TEXT)";
	
	private String SQL_LIST_ACTIVITIES_TABLE = "CREATE TABLE ListActivities " +
			"(_id INTEGER PRIMARY KEY AUTOINCREMENT, listName TEXT)";
	
	private String SQL_SMART_ACTIVITIES_TABLE = "CREATE TABLE SmartActivities (" +
			"_id INTEGER PRIMARY KEY AUTOINCREMENT," +
			"idListActivity INTEGER, " +
			" activityType TEXT, activityDesc TEXT," +
			"FOREIGN KEY(idListActivity) REFERENCES ListActivities(_id))";

	public CSmartDeviceSQLiteHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(SQL_WS_TABLE);
		db.execSQL(SQL_LOCATION_TABLE);
		db.execSQL(SQL_ACTIVITIES_TABLE);
		db.execSQL(SQL_ACTIVITY_OBSERVATION_TABLE);
		db.execSQL(SQL_LIST_ACTIVITIES_TABLE);
		db.execSQL(SQL_SMART_ACTIVITIES_TABLE);
		db.execSQL("insert into WebServices values(1,'http://181.198.93.59:8080/aqui/servlet/asd1', 300, 'aq', 'aqaction/ASD1.Execute', 'Execute',1)");
		/*db.execSQL("insert into ListActivities(listName) values('Lista 1')");
		db.execSQL("insert into ListActivities(listName) values('Lista 2')");
		db.execSQL("insert into SmartActivities(idListActivity,activityType,activityDesc) values(1,'comida','comida de la tarde')");
		db.execSQL("insert into SmartActivities(idListActivity,activityType,activityDesc) values(1,'comida','comida de la ma�ana')");
		db.execSQL("insert into SmartActivities(idListActivity,activityType,activityDesc) values(2,'deporte','deporte de la tarde')");*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
