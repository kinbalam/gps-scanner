package com.asore.csmartdevice.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.asore.csmartdevice.services.SendLocationDeviceService;

public class SendLocationReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("BroadCast", "Prepare to send data");
		Intent inService = new Intent(context, SendLocationDeviceService.class);
		context.startService(inService);
	}
}
