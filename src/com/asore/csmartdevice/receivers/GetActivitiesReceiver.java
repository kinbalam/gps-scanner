package com.asore.csmartdevice.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class GetActivitiesReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent intentActivity = new Intent("ActivitiesReady");
		intentActivity.putExtra("activities", intent.getSerializableExtra("activities"));
		LocalBroadcastManager.getInstance(context).sendBroadcast(intentActivity);
	}

}
