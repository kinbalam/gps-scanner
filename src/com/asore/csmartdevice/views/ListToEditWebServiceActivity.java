package com.asore.csmartdevice.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

import com.asore.csmartdevice.R;
import com.asore.csmartdevice.adapters.CSmartDeviceDbAdapter;
import com.asore.csmartdevice.adapters.ExpandableListAdapter;
import com.asore.csmartdevice.models.WSModel;
import com.asore.csmartdevice.services.GPSTrackerService;

@SuppressLint("NewApi")
public class ListToEditWebServiceActivity extends Activity {

	private ExpandableListAdapter listAdapter;
	private ExpandableListView expListView;
	private List<String> listDataHeader;
	private HashMap<String, List<String>> listDataChild;
	private CSmartDeviceDbAdapter dbAdapter;
	private Cursor cursor;
	private static final int ID_WS_COLUMN = 0;
	// private static final int ADDRESS_WS_COLUMN = 1;
	private static final int INTERVAL_COLUMN = 2;
	// private static final int NAMESPACE_COLUMN = 3;
	// private static final int SOAP_ACTION_COLUMN = 4;
	// private static final int METHOD_NAME_COLUMN = 5;
	private static final int CODE_OK_EDIT = 6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_web_service);
		setSettingActionBar();
		dbAdapter = new CSmartDeviceDbAdapter(getApplicationContext());
		dbAdapter.openDataBase();
		// get the listview
		expListView = (ExpandableListView) findViewById(R.id.lvExp);
		// preparing the web services
		showWebServicesList();
		listAdapter = new ExpandableListAdapter(this, listDataHeader,
				listDataChild);
		// setting list adapter
		expListView.setAdapter(listAdapter);
		setChildClickListener();
		setLongDeleteClickListener();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.ws_register_menu:
			Intent intentRegisterWS = new Intent(getApplicationContext(),
					RegisterWebServiceActivity.class);
			startActivity(intentRegisterWS);
			break;
		case R.id.ws_edit_menu:
			Intent intentEditWS = new Intent(getApplicationContext(),
					ListToEditWebServiceActivity.class);
			startActivity(intentEditWS);
			break;

		default:
			break;

		}
		return true;
	}

	private void setChildClickListener() {
		// Listview on child click listener
		expListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				if (childPosition == 0) {
					String _id_string = listDataHeader.get(groupPosition);
					String itemSelected = listDataChild.get(
							listDataHeader.get(groupPosition)).get(
							childPosition);
					Intent intent = new Intent(getApplicationContext(),
							EditWebServiceActivity.class);
					intent.putExtra("id", Integer.valueOf(_id_string));
					intent.putExtra("first_token", itemSelected);
					intent.putExtra("second_token", "");
					startActivityForResult(intent, CODE_OK_EDIT);
					return false;
				}
				if (childPosition == 2) {
					String _id_string = listDataHeader.get(groupPosition);
					int new_ws_default_id = Integer.valueOf(_id_string);
					showSetDefaultWSDialog(new_ws_default_id);
					return false;
				}
				if (childPosition == 1) {
					String _id_string = listDataHeader.get(groupPosition);
					String itemSelected = listDataChild.get(
							listDataHeader.get(groupPosition)).get(
							childPosition);
					StringTokenizer tokens = new StringTokenizer(itemSelected,
							":");
					String first_token = tokens.nextToken();
					String second_token = tokens.nextToken().trim();
					while (tokens.hasMoreTokens()) {
						String extraToken = tokens.nextToken();
						second_token += ":" + extraToken;
					}
					Intent intent = new Intent(getApplicationContext(),
							EditWebServiceActivity.class);
					intent.putExtra("id", Integer.valueOf(_id_string));
					intent.putExtra("first_token", first_token);
					intent.putExtra("second_token", second_token);
					startActivityForResult(intent, CODE_OK_EDIT);
					return false;
				}
				return false;
			}
		});
	}

	private void showSetDefaultWSDialog(final int new_default_ws_id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				ListToEditWebServiceActivity.this);
		String title = "Establecer Web Service Predeterminado";
		String msg = "Establecer para envio de localizacion \n"
				+ new_default_ws_id;
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton("Establecer",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dbAdapter.openDataBase();
						int rowsAfected = dbAdapter
								.updateDefaultWebService(new_default_ws_id);
						resetServiceGPS();
						if (rowsAfected == 1) {
							Toast.makeText(getApplicationContext(),
									"Se ha establecido correctamente",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(getApplicationContext(),
									"No se pudo establecer por defecto",
									Toast.LENGTH_LONG).show();
						}
						dbAdapter.closeConnectionDB();
					}
				}).setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		builder.show();
	}

	private void setSettingActionBar() {
		// enable the home button
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#1CA5EC")));
	}

	private void setLongDeleteClickListener() {
		expListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
					// Your code with group long click
//					String itemSelected = listDataChild.get(
//							listDataHeader.get(position)).get(0);

					int ws_ID = Integer.valueOf(listDataHeader.get(position));
					showOptionsAlert(ws_ID);
					return true;
				}

				return false;
			}
		});
	}

	private void showOptionsAlert(final int ws_id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				ListToEditWebServiceActivity.this);
		String title = "Eliminar Web Service";
		String msg = "Seguro que desea eliminar el Web service con indice: "
				+ ws_id;
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton("Eliminar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dbAdapter.openDataBase();
						Cursor ws_cursor = dbAdapter.getWebServicesCursor();
						if (ws_cursor.getCount() == 1) {
							Toast.makeText(
									getApplicationContext(),
									"Solo existe 1 web service registrado y esta predeterminado, no puedes borrarlo.",
									Toast.LENGTH_LONG).show();
						} else {
							WSModel defaultWS = dbAdapter
									.getDefaultWebService();
							if (defaultWS.getID() == ws_id) {
								Toast.makeText(
										getApplicationContext(),
										"No puedes borrar el web service por defecto, " +
										"establece otro por defecto e intenta de nuevo.",
										Toast.LENGTH_LONG).show();
							}
							else{
								int rows_affected = dbAdapter.deleteWS(ws_id);
								if (rows_affected != 0) {
									updateExpandableList();
									Toast.makeText(
											getApplicationContext(),
											"Se ha eliminado " + rows_affected
													+ " registro(s).",
											Toast.LENGTH_LONG).show();
								} else {
									Toast.makeText(
											getApplicationContext(),
											"No se ha podido eliminar el registro.",
											Toast.LENGTH_LONG).show();
								}
							}
							
						}

						dbAdapter.closeConnectionDB();
					}
				}).setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		builder.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_web, menu);
		return true;
	}

	/*
	 * Preparing web services list
	 */
	private void showWebServicesList() {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();
		cursor = dbAdapter.getWebServicesCursor();
		// adding url's from cursor to list header
		if (cursor.moveToFirst()) {
			do {
				int _id = cursor.getInt(ID_WS_COLUMN);
				// String ws_url = cursor.getString(ADDRESS_WS_COLUMN);
				int seconds = Integer
						.valueOf(cursor.getString(INTERVAL_COLUMN));
				// String namespace = cursor.getString(NAMESPACE_COLUMN);
				// String soap_action = cursor.getString(SOAP_ACTION_COLUMN);
				// String method_name = cursor.getString(METHOD_NAME_COLUMN);
				listDataHeader.add(String.valueOf(_id));
				List<String> ws_information = new ArrayList<String>();
				ws_information.add("Actualizar URL");
				// ws_information.add("Namespace: " + namespace);
				// ws_information.add("Soap Action: " + soap_action);
				// ws_information.add("Metodo: " + method_name);
				ws_information.add("Intervalo de envio: " + seconds);
				ws_information.add("Establecer Web Service por defecto");
				listDataChild.put(String.valueOf(_id), ws_information);
			} while (cursor.moveToNext());
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check which request we're responding to
		if (requestCode == CODE_OK_EDIT) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				updateExpandableList();
			}
		}

	}

	private void updateExpandableList() {
		showWebServicesList();
		listAdapter = new ExpandableListAdapter(this, listDataHeader,
				listDataChild);
		// setting list adapter
		expListView.setAdapter(listAdapter);
	}

	private void resetServiceGPS() {
		Intent intentService = new Intent(getApplicationContext(),
				GPSTrackerService.class);
		stopService(intentService);
		startService(intentService);
	}

}
