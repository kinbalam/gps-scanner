package com.asore.csmartdevice.views;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.Toast;

import com.asore.csmartdevice.R;
import com.asore.csmartdevice.adapters.CSmartDeviceDbAdapter;
import com.asore.csmartdevice.models.WSModel;

@SuppressLint("NewApi")
public class RegisterWebServiceActivity extends Activity {
	private EditText textURL;
	private EditText textNameSpace;
	private EditText textSoap_Action;
	private EditText textMethod_Name;
	private EditText textSeconds;
	private WSModel webService;
	private CSmartDeviceDbAdapter dbAdapter;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_ws);
		dbAdapter = new CSmartDeviceDbAdapter(getApplicationContext());
		textSeconds = (EditText) findViewById(R.id.editText_send_coord);
		setKeyListenerOnEditText();
		setSettingActionBar();
	}

	private void setKeyListenerOnEditText() {
		// TODO Auto-generated method stub
		textSeconds.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				// TODO Auto-generated method stub
				String secondsString = textSeconds.getText().toString();
				int seconds = 0;
				if (!secondsString.equals("")) {
					seconds = Integer.valueOf(secondsString);
					if (seconds < 10) {
						textSeconds.setText("10");
					}
				}
				return false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register_w, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.ws_register_menu:
			Intent intentRegisterWS = new Intent(getApplicationContext(),
					RegisterWebServiceActivity.class);
			startActivity(intentRegisterWS);
			break;
		case R.id.ws_edit_menu:
			Intent intentEditWS = new Intent(getApplicationContext(),
					ListToEditWebServiceActivity.class);
			startActivity(intentEditWS);
			break;

		default:
			break;

		}
		return true;
	}

	public void registerWebService(View view) {
		if (!emptyTexts()) {
			dbAdapter.openDataBase();
			webService = getWS_Model();
			if (URLUtil.isHttpUrl(webService.getUrl())) {
				StringTokenizer tokens = new StringTokenizer(
						webService.getUrl(), "//");
				@SuppressWarnings("unused")
				String first_token = tokens.nextToken();
				String second_token_ip = tokens.nextToken(":")
						.replace("//", "");
				if (isIpValid(second_token_ip)) {
					Log.i("register", "is valid url");
					ContentValues newRegister = new ContentValues();
					newRegister.put("url", webService.getUrl());
					newRegister.put("namespace", webService.getNamespace());
					newRegister.put("intervaloDeEnvio",
							webService.getInterval_coordinates());
					newRegister.put("method_name", webService.getMethod_name());
					newRegister.put("soap_action", webService.getSoap_action());
					long newID_Register = dbAdapter
							.registerNewWebService(newRegister);
					if (newID_Register == -1) {
						Toast.makeText(
								getApplicationContext(),
								"Error de registro en base de datos. Intente nuevamente",
								Toast.LENGTH_LONG).show();
					} else {
						showOptionsAlert(newID_Register);
					}
				} else {
					Log.i("Error", "no valid ip");
				}
			} else {
				Log.i("error", "no valid url");
			}

		} else {
			Toast.makeText(getApplicationContext(),
					"Todos los campos son necesarios para el registro.",
					Toast.LENGTH_LONG).show();
		}

	}

	private boolean emptyTexts() {
		boolean empty = false;
		WSModel ws = getWS_Model();
		if (ws.getInterval_coordinates() == 0 || ws.getMethod_name().equals("")
				|| ws.getNamespace().equals("")
				|| ws.getSoap_action().equals("") || ws.getUrl().equals("")) {
			empty = true;
		}
		return empty;
	}

	public void returnMain(View view) {
		setResult(RESULT_CANCELED);
		finish();
	}

	private boolean isIpValid(String ip) {
		boolean valid = false;
		Pattern IP_ADDRESS = Pattern
				.compile("((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
						+ "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
						+ "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
						+ "|[1-9][0-9]|[0-9]))");
		Matcher matcher = IP_ADDRESS.matcher(ip);
		if (matcher.matches()) {
			// ip is correct
			valid = true;
		}
		return valid;
	}

	private WSModel getWS_Model() {
		textURL = (EditText) findViewById(R.id.editText_URL);
		textNameSpace = (EditText) findViewById(R.id.editText_namespace);
		textMethod_Name = (EditText) findViewById(R.id.editText_method);
		textSeconds = (EditText) findViewById(R.id.editText_send_coord);
		textSoap_Action = (EditText) findViewById(R.id.editText_soap_action);
		String url = textURL.getText().toString();
		String nameSpace = textNameSpace.getText().toString();
		String method = textMethod_Name.getText().toString();
		String secondsString = textSeconds.getText().toString();
		int seconds = 0;
		if (!secondsString.equals("")) {
			seconds = Integer.valueOf(secondsString);
		}
		String soapAction = textSoap_Action.getText().toString();
		webService = new WSModel(0, nameSpace, url, method, soapAction, seconds);
		return webService;
	}

	private void showOptionsAlert(long newID_Register) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				RegisterWebServiceActivity.this);
		String title = "Registro exitoso";
		String msg = "El identificador de su nuevo Web Service es: "
				+ newID_Register + ".\n"
				+ "Le servira como identificador unico de su nuevo WS.";
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton("Agregar nuevo",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						clearText();
					}
				}).setNegativeButton("Volver a menu principal",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dbAdapter.closeConnectionDB();
						finish();
					}
				});
		builder.show();
	}

	private void clearText() {
		textURL.setText("");
		textNameSpace.setText("");
		textMethod_Name.setText("");
		textSeconds.setText("");
		textSoap_Action.setText("");
		textURL.requestFocus();
	}

	private void setSettingActionBar() {
		// enable the home button
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#1CA5EC")));
	}

}
