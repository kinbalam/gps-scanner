package com.asore.csmartdevice.views;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.asore.csmartdevice.R;
import com.asore.csmartdevice.adapters.CSmartDeviceDbAdapter;
import com.asore.csmartdevice.adapters.NavDrawerListAdapter;
import com.asore.csmartdevice.database.CSmartDeviceSQLiteHelper;
import com.asore.csmartdevice.models.NavDrawerItemModel;
import com.asore.csmartdevice.models.WSModel;
import com.asore.csmartdevice.receivers.SendLocationReceiver;
import com.asore.csmartdevice.services.GPSTrackerService;

@SuppressLint("NewApi")
public class MainActivity extends Activity{

	private static final String NAME_DB = "CSmartDevices";
	private CSmartDeviceSQLiteHelper ws_helper;
	private CSmartDeviceDbAdapter dbAdapter;
	private WSModel wsModel;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	// nav drawer title
	private CharSequence mDrawerTitle;
	// used to store app title
	private CharSequence mTitle;
	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItemModel> navDrawerItems;
	private NavDrawerListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setNavigationDrawer();
		initPreferences();
		setAdapterDBSettings();
		// setAlarmManagerService();
		Intent serviceIntent = new Intent(getApplicationContext(),
				GPSTrackerService.class);
		startService(serviceIntent);
	}

	private void setAdapterDBSettings() {
		ws_helper = new CSmartDeviceSQLiteHelper(getApplicationContext(),
				NAME_DB, null, 1);
		ws_helper.getWritableDatabase();
		dbAdapter = new CSmartDeviceDbAdapter(getApplicationContext());
		dbAdapter.openDataBase();
		wsModel = dbAdapter.getDefaultWebService();
	}

	@SuppressWarnings("unused")
	private void setAlarmManagerService() {
		AlarmManager alarms = (AlarmManager) this
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(getApplicationContext(),
				SendLocationReceiver.class);
		final PendingIntent pIntent = PendingIntent.getBroadcast(this, 1234567,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		int intervalo = 1000 * wsModel.getInterval_coordinates();
		alarms.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis(), intervalo, pIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case R.id.ws_register_menu:
			Intent intentRegisterWS = new Intent(getApplicationContext(),
					RegisterWebServiceActivity.class);
			startActivity(intentRegisterWS);
			break;
		case R.id.ws_edit_menu:
			Intent intentEditWS = new Intent(getApplicationContext(),
					ListToEditWebServiceActivity.class);
			startActivity(intentEditWS);
			break;

		default:
			break;

		}
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private void setNavigationDrawer() {
		mTitle = mDrawerTitle = getTitle();
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#1CA5EC")));
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItemModel>();

		// adding nav drawer items to array
		// Home
		// navDrawerItems.add(new NavDrawerItemModel(navMenuTitles[0],
		// navMenuIcons.getResourceId(0, -1)));
		// add ws
		navDrawerItems.add(new NavDrawerItemModel(navMenuTitles[0],
				navMenuIcons.getResourceId(0, -1)));
		// settings ws
		navDrawerItems.add(new NavDrawerItemModel(navMenuTitles[1],
				navMenuIcons.getResourceId(1, -1)));
		// enable
		navDrawerItems.add(new NavDrawerItemModel(navMenuTitles[2],
				navMenuIcons.getResourceId(2, -1)));
		// state
		navDrawerItems.add(new NavDrawerItemModel(navMenuTitles[3],
				navMenuIcons.getResourceId(3, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.arrow, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.ws_register_menu).setVisible(!drawerOpen);
		menu.findItem(R.id.ws_edit_menu).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		switch (position) {
		case 0:
			Intent intentRegisterWS = new Intent(getApplicationContext(),
					RegisterWebServiceActivity.class);
			startActivity(intentRegisterWS);
			break;
		case 1:
			Intent intentEditWS = new Intent(getApplicationContext(),
					ListToEditWebServiceActivity.class);
			startActivity(intentEditWS);
			break;
		case 2:
			showEnableDialog();
			break;
		case 3:
			showStateEnableDialog();
			break;
		default:
			break;
		}
	}

	private void showStateEnableDialog() {
		final SharedPreferences prefs = getSharedPreferences("MisPreferencias",
				Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		String enable = prefs.getString("enable", "1");
		if (enable == null) {
			editor.putString("enable", "1");
			editor.commit();
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		String title = "C-SmartDevice";
		String message = "Enable: " + enable;
		builder.setCancelable(false);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				mDrawerToggle.syncState();
				dialog.dismiss();
			}
		});
		builder.show();
	}

	private void showEnableDialog() {
		final SharedPreferences prefs = getSharedPreferences("MisPreferencias",
				Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		String title = "C-SmartDevice";
		builder.setCancelable(false);
		builder.setTitle(title);
		builder.setPositiveButton("1", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				editor.putString("enable", "1");
				editor.commit();
				Intent intentService = new Intent(getApplicationContext(),
						GPSTrackerService.class);
				startService(intentService);
			}
		}).setNegativeButton("0", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				editor.putString("enable", "0");
				editor.commit();
				Intent intentService = new Intent(getApplicationContext(),
						GPSTrackerService.class);
				stopService(intentService);
			}
		});
		builder.show();
	}

	private void initPreferences() {
		final SharedPreferences prefs = getSharedPreferences("MisPreferencias",
				Context.MODE_PRIVATE);
		final SharedPreferences.Editor editor = prefs.edit();
		if (prefs.getString("enable", "0") == null) {
			editor.putString("enable", "1");
			editor.commit();
		}

	}
}
