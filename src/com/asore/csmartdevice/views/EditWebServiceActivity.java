package com.asore.csmartdevice.views;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.asore.csmartdevice.R;
import com.asore.csmartdevice.adapters.CSmartDeviceDbAdapter;
import com.asore.csmartdevice.services.GPSTrackerService;

@SuppressLint("NewApi")
public class EditWebServiceActivity extends Activity {

	private CSmartDeviceDbAdapter dbAdapter;
	private TextView view;
	private EditText text;
	private int ws_id;
	private String item_WS;
	private Intent intent;
	private String last_ws_info;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);
		setSettingActionBar();
		intent = getIntent();
		ws_id = intent.getIntExtra("id", 0);
		setWebServiceInformation();
		dbAdapter = new CSmartDeviceDbAdapter(getApplicationContext());
		dbAdapter.openDataBase();
	}

	private void setKeyListenerOnEditText() {
		// TODO Auto-generated method stub
		text.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				// TODO Auto-generated method stub
				String secondsString = text.getText().toString();
				int seconds = 0;
				if (arg2.getKeyCode() != KeyEvent.KEYCODE_DEL) {
					if (!secondsString.equals("")) {
						seconds = Integer.valueOf(secondsString);
						if (seconds < 10) {
							text.setText("10");
						}
					}
				}

				return false;
			}
		});
	}

	public void saveWebService(View view) {
		if (noEmptyText()) {
			text = (EditText) findViewById(R.id.editText_seconds);
			String new_ws_info = text.getText().toString();
			if (new_ws_info.equals(last_ws_info)) {
				Log.i("No cambios", "no hiciste cambios");
			} else {
				int rowsAffected = 0;
				// Establecemos los campos-valores a actualizar
				ContentValues valuesUpdate = new ContentValues();
				if (item_WS.equals("Actualizar URL")) {
					if (URLUtil.isHttpUrl(new_ws_info)) {
						StringTokenizer tokens = new StringTokenizer(
								new_ws_info, "//");
						@SuppressWarnings("unused")
						String first_token = tokens.nextToken();
						String second_token_ip = tokens.nextToken(":").replace(
								"//", "");
						if (isIpValid(second_token_ip)) {
							valuesUpdate.put("url", new_ws_info);
							rowsAffected = dbAdapter.updateWebService(
									valuesUpdate, ws_id);
							Log.i("Update", "is valid url");
						} else {
							Log.i("No Update", "no valid ip");
						}
					} else {
						Log.i("No Update", "no valid url");
					}
				}
				if (item_WS.equals("Intervalo de envio")
						&& new_ws_info.matches("[0-9]*")) {
					int seconds = Integer.valueOf(new_ws_info);
					valuesUpdate.put("intervaloDeEnvio", seconds);
					rowsAffected = dbAdapter.updateWebService(valuesUpdate,
							ws_id);
					resetServiceGPS();
				}
				if (rowsAffected != 0) {
					showOptionsAlert();
					Log.i("Updated", "Web service actualizado");
				} else {
					// Toast.makeText(getApplicationContext(),
					// "No se pudo actualizar la informacion.",
					// Toast.LENGTH_LONG).show();
					Log.i("Error", "No se pudo actualizar la informacion");
				}

			}
		} else {
			Toast.makeText(getApplicationContext(),
					"No puede actualizar a un campo vacio.", Toast.LENGTH_LONG)
					.show();
		}

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.ws_register_menu:
			Intent intentRegisterWS = new Intent(getApplicationContext(),
					RegisterWebServiceActivity.class);
			startActivity(intentRegisterWS);
			break;
		case R.id.ws_edit_menu:
			Intent intentEditWS = new Intent(getApplicationContext(),
					ListToEditWebServiceActivity.class);
			startActivity(intentEditWS);
			break;

		default:
			break;

		}
		return true;
	}

	private void setSettingActionBar() {
		// enable the home button
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#1CA5EC")));
	}

	private boolean noEmptyText() {
		boolean noEmpty = true;
		text = (EditText) findViewById(R.id.editText_seconds);
		if (text.getText().toString().equals("")) {
			noEmpty = false;
		}
		return noEmpty;
	}

	private void showOptionsAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				EditWebServiceActivity.this);
		String title = "Actualizacion exitosa";
		String msg = "Se ha actualizado correctamente el Web Service.";
		builder.setCancelable(false);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setPositiveButton("Volver a modificar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				}).setNegativeButton("Regresar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						setResult(RESULT_OK);
						finish();
					}
				});
		builder.show();
	}

	private void setWebServiceInformation() {
		view = (TextView) findViewById(R.id.textView_first);
		text = (EditText) findViewById(R.id.editText_seconds);
		item_WS = intent.getStringExtra("first_token");
		last_ws_info = intent.getStringExtra("second_token");
		view.setText(item_WS);
		if (item_WS.equals("Actualizar URL")) {
			text.setText("");
		}
		if (item_WS.equals("Intervalo de envio")) {
			text.setInputType(InputType.TYPE_CLASS_NUMBER);
			text.setText(last_ws_info);
			setKeyListenerOnEditText();
		}
		// text.setText(last_ws_info);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit, menu);
		return true;
	}

	public void returnMain(View view) {
		dbAdapter.closeConnectionDB();
		setResult(RESULT_OK);
		finish();
	}

	private void resetServiceGPS() {
		Intent intentService = new Intent(getApplicationContext(),
				GPSTrackerService.class);
		stopService(intentService);
		startService(intentService);
	}

	private boolean isIpValid(String ip) {
		boolean valid = false;
		Pattern IP_ADDRESS = Pattern
				.compile("((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
						+ "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
						+ "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
						+ "|[1-9][0-9]|[0-9]))");
		Matcher matcher = IP_ADDRESS.matcher(ip);
		if (matcher.matches()) {
			// ip is correct
			valid = true;
		}
		return valid;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		dbAdapter.closeConnectionDB();
	}

}
