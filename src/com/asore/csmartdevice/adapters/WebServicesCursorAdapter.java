package com.asore.csmartdevice.adapters;



import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WebServicesCursorAdapter extends CursorAdapter {

	private static final String COLUMN_URL_WS = "url";
	private CSmartDeviceDbAdapter dbAdapter = null;

	@SuppressWarnings("deprecation")
	public WebServicesCursorAdapter(Context context, Cursor c) {
		super(context, c);
		dbAdapter = new CSmartDeviceDbAdapter(context);
		dbAdapter.openDataBase();
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView tv = (TextView) view;
		tv.setText(cursor.getString(cursor.getColumnIndex(COLUMN_URL_WS)));
	}

	@SuppressLint("InlinedApi")
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(context);
		final View view = inflater.inflate(android.R.layout.simple_list_item_1,
				parent, false);

		return view;
	}

}
