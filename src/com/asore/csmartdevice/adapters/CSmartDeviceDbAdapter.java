package com.asore.csmartdevice.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.asore.csmartdevice.database.CSmartDeviceSQLiteHelper;
import com.asore.csmartdevice.models.WSModel;

public class CSmartDeviceDbAdapter{


	private static final String DBName = "CSmartDevices";
	private static final String COLUMN_ID = "_id";
	private static final String WEB_SERVICES_TABLE = "WebServices";
	private static final String COLUMN_URL_WEBSERVICES = "url";
	private static final String COLUMN_INTERVAL_SENDCORDINATES = "intervaloDeEnvio";
	private static final String COLUMN_SOAP_ACTION = "soap_action";
	private static final String COLUMN_METHOD_NAME = "method_name";
	private static final String COLUMN_NAMESPACE = "namespace";
	// Columns for table location
	private static final String LOCATION_TABLE = "Location";
	private static final String COLUMN_LATITUDE = "latitud";
	private static final String COLUMN_LONGITUDE = "longitud";
	private static final String COLUMN_IMEI = "imei";
	private static final String COLUMN_DATE = "fecha";
	private static final String NUMBER_OPR_COLUMN = "numOpr";
	// index columns for ws table
	private static final int ID_WS_COLUMN = 0;
	private static final int ADDRESS_WS_COLUMN = 1;
	private static final int INTERVAL_COLUMN = 2;
	private static final int NAMESPACE_COLUMN = 3;
	private static final int SOAP_ACTION_COLUMN = 4;
	private static final int METHOD_NAME_COLUMN = 5;
	// columns for table activities
	private static final String ACTIVITIES_TABLE = "Activities";
	private static final String COLUMN_TYPE_ACTIVITY = "activityType";
	private static final String COLUMN_DESC_ACTIVITY = "activityDesc";
	//columns for table activity_observation
	private static final String OBSERVATIONS_TABLE = "Observations";
	private static final String COLUMN_OBSERVATION = "observation";
	private Context contexto;
	private CSmartDeviceSQLiteHelper dbHelper;
	private SQLiteDatabase db;

	/**
	 * Definimos lista de columnas de la tabla para utilizarla en las consultas
	 * a la base de datos
	 */
	private String[] ws_columns = new String[] { COLUMN_ID,
			COLUMN_URL_WEBSERVICES, COLUMN_INTERVAL_SENDCORDINATES,
			COLUMN_NAMESPACE, COLUMN_SOAP_ACTION, COLUMN_METHOD_NAME };

	private String[] location_columns = new String[] { COLUMN_ID,
			COLUMN_LATITUDE, COLUMN_LONGITUDE, COLUMN_IMEI, COLUMN_DATE,
			NUMBER_OPR_COLUMN, COLUMN_TYPE_ACTIVITY, COLUMN_OBSERVATION};
	
	private String[] activities_columns = new String[] {COLUMN_ID,
			COLUMN_TYPE_ACTIVITY,COLUMN_DESC_ACTIVITY};
	
	private String[] observations_columns = new String[] {COLUMN_ID,
			COLUMN_TYPE_ACTIVITY,COLUMN_OBSERVATION};

	public CSmartDeviceDbAdapter(Context context) {
		this.contexto = context;
	}

	public CSmartDeviceDbAdapter openDataBase() throws SQLException {
		dbHelper = new CSmartDeviceSQLiteHelper(contexto, DBName, null, 1);
		db = dbHelper.getWritableDatabase();
		return this;
	}

	public void closeConnectionDB() {
		dbHelper.close();
	}

	/**
	 * Devuelve cursor con todos las columnas de la tabla
	 */
	public Cursor getWebServicesCursor() throws SQLException {
		Cursor c = db.query(true, WEB_SERVICES_TABLE, ws_columns, null, null,
				null, null, null, null);
		return c;
	}

	public Cursor getLocationsCursor() throws SQLException {
		Cursor c = db.query(true, LOCATION_TABLE, location_columns, null, null,
				null, null, null, null);
		return c;
	}
	
	public Cursor getActivities() throws SQLException{
		Cursor c = db.query(true, ACTIVITIES_TABLE, activities_columns, null, null,
				null, null, null, null);
		return c;
	}
	
	public Cursor getObservations() throws SQLException{
		Cursor c = db.query(true, OBSERVATIONS_TABLE, observations_columns, null, null,
				null, null, null, null);
		return c;
	}

	public long registerNewWebService(ContentValues newRegister) {
		return db.insert(WEB_SERVICES_TABLE, null, newRegister);
	}
	
	public long registerActivity(ContentValues newRegister) {
		return db.insert(ACTIVITIES_TABLE, null, newRegister);
	}
	
	public long registerObservation(ContentValues newRegister){
		return db.insert(OBSERVATIONS_TABLE, null, newRegister);
	}

	public int updateWebService(ContentValues updateRegister, int wsID) {
		return db.update(WEB_SERVICES_TABLE, updateRegister, "_id=" + wsID,
				null);
	}

	public int updateObservation(ContentValues updateRegister, int observationID) {
		return db.update(OBSERVATIONS_TABLE, updateRegister, "_id=" + observationID,
				null);
	}
	
	public int deleteWS(int ws_id) {
		return db.delete(WEB_SERVICES_TABLE, "_id=" + ws_id, null);
	}
	
	public int deleteObservation(int idObservation){
		return db.delete(OBSERVATIONS_TABLE, "_id=" + idObservation, null);
	}

	public long registerNewLocation(ContentValues newLocation) {
		return db.insert(LOCATION_TABLE, null, newLocation);
	}

	public int deleteLocation(String num_opr) {
		return db.delete(LOCATION_TABLE, "numOpr= '" + num_opr + "'", null);
	}

	public WSModel getDefaultWebService() {
		WSModel wsModel = null;
		final int id;
		String namespace;
		String url;
		String soap_action;
		String method;
		int interval;
		Cursor c = db.rawQuery("select * from " + WEB_SERVICES_TABLE
				+ " where def = 1 ", null);
		int numrows = c.getCount();
		if (c.moveToNext() && numrows == 1) {
			id = c.getInt(ID_WS_COLUMN);
			namespace = c.getString(NAMESPACE_COLUMN);
			url = c.getString(ADDRESS_WS_COLUMN);
			soap_action = c.getString(SOAP_ACTION_COLUMN);
			method = c.getString(METHOD_NAME_COLUMN);
			interval = c.getInt(INTERVAL_COLUMN);
			wsModel = new WSModel(id, namespace, url, method, soap_action,
					interval);
		}
		return wsModel;
	}

	public int updateDefaultWebService(int new_default_ws_id) {
		WSModel actual_default_wsModel = getDefaultWebService();
		quitDefaultWebService(actual_default_wsModel.getID());
		return setDefaultWebService(new_default_ws_id);		
	}
	
	private int setDefaultWebService(int new_default_ws_id) {
		ContentValues updateDefWs = new ContentValues();
		updateDefWs.put("def", 1);
		return db.update(WEB_SERVICES_TABLE, updateDefWs, "_id=" + new_default_ws_id,null);	
	}

	private void quitDefaultWebService(int actual_default_wsID){
		ContentValues defaultWS_Values = new ContentValues();
		defaultWS_Values.put("def", 0);
		db.update(WEB_SERVICES_TABLE, defaultWS_Values, "_id=" + actual_default_wsID, null);
	}

	public int deleteActivity(int id_activity) {
		return db.delete(ACTIVITIES_TABLE, "_id=" + id_activity, null);
	}
}
